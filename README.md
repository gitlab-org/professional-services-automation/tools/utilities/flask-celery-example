# Flask with Celery

...and mongo, redis, and flower

Sample app to cover integrating Celery with Flask, using Redis as the Celery broker,
MongoDB as the result store, and Flower to monitor Celery

## Dependencies

- [Poetry](https://python-poetry.org/)
- Something to run containers (docker, podman, etc)
- Docker compose

## Usage

### Clone this repo

```bash
git clone <this-repo>
cd <this-repo>
mkdir cache
```

### Install python dependencies

```bash
poetry install
```

### Spin up Mongo and Redis through docker compose

```bash
# with docker
docker-compose up

# with podman
docker-compose -H unix:///run/podman/podman.sock up
```

### Spin up Celery and Flower

```bash
## Celery
poetry run celery -A celery_flask.wsgi.celery_app worker --loglevel=INFO

## Flower
poetry run celery -A celery_flask.wsgi.celery_app flower --port=5566
```

### Spin up Flask

```bash
export FLASK_APP=celery_flask/wsgi
FLASK_DEBUG=1 poetry run flask run

```

### Testing this out

- Make a request to `http://localhost:5000/test` to trigger a 'hello world' task
    - Expected output should look like:
    ```json
        {
            "result": "cfd29944-fcd4-4865-96d9-4ae8acbcb1fd"
        }
    ```
- There should be an ID included in the API response. Copy that for the next step
- Make a request to `http://localhost:5000/test/:id` where `:id` is the ID copied from the previous request
    - Expected out should look like:
    ```json
    {
        "ready": true,
        "successful": true,
        "value": "hello world"
    }
    ```
- Navigate to `http://localhost:5566` to view the Flower dashboard and check for the task you created through the Flask API

## Resources

- https://flask.palletsprojects.com/en/2.3.x/patterns/celery/
- https://flower.readthedocs.io/en/latest/index.html
- https://docs.celeryq.dev/en/stable/getting-started/introduction.html
- https://stackoverflow.com/questions/25884951/attributeerror-flask-object-has-no-attribute-user-options
