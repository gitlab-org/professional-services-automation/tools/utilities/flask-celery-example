
from flask import Flask


from celery import Celery, Task
from celery.result import AsyncResult
from celery_flask.tasks import hello

def celery_init_app(app: Flask) -> Celery:
    class FlaskTask(Task):
        def __call__(self, *args: object, **kwargs: object) -> object:
            with app.app_context():
                return self.run(*args, **kwargs)

    celery_app = Celery(app.name, task_cls=FlaskTask)
    celery_app.config_from_object(app.config["CELERY"])
    celery_app.set_default()
    app.extensions["celery"] = celery_app
    return celery_app

app = Flask(__name__)
app.config.from_mapping(
    CELERY=dict(
        broker_url="redis://:password@localhost:6379/0",
        result_backend="mongodb://localhost:27017/jobs",
        task_ignore_result=True
    ),
)
celery_app = celery_init_app(app)


@app.route('/test')
def test():
    result = hello.delay()
    return {'result': result.id}

@app.route('/test/<rid>')
def get_result(rid):
    result = AsyncResult(rid)
    return {
        "ready": result.ready(),
        "successful": result.successful(),
        "value": result.result if result.ready() else None,
    }